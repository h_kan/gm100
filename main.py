#!/usr/bin/env python

import numpy as np
import sys
from sub_progs import gen
from sub_progs import check_cmb
from sub_progs import config_params

def run(i, ref):
	print "-----",i+1,"-----"
	gen.gen_start(i+1, ref)
	#gen.gen_start2(i+1, ref)


if __name__=='__main__':
	if len(sys.argv) == 1:
		filename = 'config.ini'
	else:
		filename = sys.argv[1]
	
	ref = config_params.Refs(filename)

	if ref.gen_fg:
		from sub_progs import call_gmfg
		call_gmfg.generate_foreground(ref)
	
	N = ref.repeat_num
	check = ref.check

	for i in range(N):
		run(i, ref)

	print "------ generation has finished ------"

	if check:
		check_cmb.check(ref.expected_cmb, ref.generated_cmb, N)

