#!/usr/bin/env python

import numpy as np
import healpy as hp
import os

from ConfigParser import SafeConfigParser

# read config and set parameters
class Refs:
	def __init__(self,filename):
		main_dir = os.path.abspath(os.path.dirname(__file__))+"/../"

		self.input_dir = main_dir + "./input/"
		### read input data config
		maps_parser = SafeConfigParser()
		maps_parser.read(self.input_dir + "input_data_config.ini")

		### read config.ini
		parser = SafeConfigParser()
		parser.read(main_dir + filename)
	

		## [process]
		self.bool_cmb = parser.getboolean('process', 'generate_cmb')
		self.bool_smooth = parser.getboolean('process', 'smooth_cmb')
		self.bool_noise = parser.getboolean('process', 'add_noise')
		self.bool_fg = parser.getboolean('process', 'add_foreground')

		self.repeat_num = parser.getint('process', 'repeat_num')

		if self.bool_cmb:
			## [cmb]
			cl_dir = "./input/" + maps_parser.get('cmb','cl_dir')
			self.cl_lensed = cl_dir + maps_parser.get('cmb','cl_lensed')
			self.cl_scalar = cl_dir + maps_parser.get('cmb','cl_scalar')
			self.cl_tensor = cl_dir + maps_parser.get('cmb','cl_tensor')
	
			## [cmb]
			self.lense = parser.getboolean('cmb','lense')
			self.r = parser.getfloat('cmb','r')
			self.seed1 = parser.getint('cmb','cmb_seed')
	
		
		if self.bool_fg:
			## [foreground]
			self.gen_fg = parser.getboolean('foreground','generate_foreground')
			self.fg_input_dir = main_dir + parser.get('foreground','fg_dir')
	
			if self.gen_fg:
				### if gen_fg = True at config.ini, read fg_config.ini
				fg_parser = SafeConfigParser()
				fg_parser.read(main_dir + './gmFG/'+parser.get('foreground','config'))
				self.parserFG = fg_parser

				self.fg_output_dir = main_dir + './gmFG/' + fg_parser.get('output','output_dir')
				fg_name_source = self.fg_output_dir + fg_parser.get('output','name')
				self.fg_pre = fg_parser.get('output','last_foreground_comp_name')
				if fg_parser.getboolean('output','degrade'):
					self.fg_nside = fg_parser.getint('output','nside_out')
				else:
					self.fg_nside = fg_parser.getint('input','nside')
				self.fg_name = fg_name_source.replace('[comp]', self.fg_pre).replace('[nside]',str(self.fg_nside))


				self.fg_nu = np.array([float(i) for i in fg_parser.get('input','frequency').split()])
				self.fg_smooth = fg_parser.getboolean('output','smooth')
				self.fg_fwhm = np.array([float(i) for i in fg_parser.get('output','fwhm').split()])

			else:
				self.fg_name = self.fg_input_dir + parser.get('foreground','fg_name')
		else:
			self.gen_fg = False

		
		## [par]
		self.nside = parser.getint('par','nside')
		self.nu = np.array([float(i) for i in parser.get('par','nu').split()])
		self.noise = np.array([float(i) for i in parser.get('par','noise').split()])
		self.fwhm = np.array([float(i) for i in parser.get('par','fwhm').split()])
		self.seed2 = parser.getint('par','noise_seed')
	
	
	
		## [output]
		self.output_dir = parser.get('output','output_dir')
		self.map_name = self.output_dir + parser.get('output','map_name')

		self.output_cmb = parser.getboolean('output','output_cmb')
		if self.output_cmb:
			self.cmb_name = self.output_dir + parser.get('output','cmb_name')
		self.output_smoothed_cmb = parser.getboolean('output','output_smoothed_cmb')
		if self.output_smoothed_cmb:
			self.smoothed_cmb_name = self.output_dir + parser.get('output','smoothed_cmb_name')
		self.output_noise = parser.getboolean('output','output_noise')
		if self.output_noise:
			self.noise_name = self.output_dir + parser.get('output','noise_name')
	
	
		## [check]
		self.check = parser.getboolean('check', 'check')
		if self.check:
			self.check_name = self.output_dir + parser.get('check', 'check_name')
			check_dir = self.input_dir + maps_parser.get('check','check_dir')
			self.expected_cmb = check_dir + maps_parser.get('check','check_name')
			self.generated_cmb = self.output_dir + parser.get('check','check_name')
		
	
