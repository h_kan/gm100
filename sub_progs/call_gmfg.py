#!/usr/bin/env python

import numpy as np
import sys
import os

# run gmFG
def run_gmfg(ref):
	sys.path.append(os.path.abspath(os.path.dirname(__file__)) + '/../gmFG/sub_progs_fg')
	import gen_fg
	ref_fg = gen_fg.Refs(ref.parserFG)
	gen_fg.generate(ref_fg)
	
class CfgMismatchError(Exception):
	def __init__(self, val):
		self.val = val
	def __str__(self):
		return repr(self.val)

# check if parameters (frequency, beam size) set at config.ini of gm100 equal to at config_fg.ini of gmFG
# If set frequency is not equivalent, error occur and stop running. 
# However, if FWHM of beam size is not equivalent, just a warning is displayed.
def check_params(ref):
	nu100  = ref.nu
	nuFG = ref.fg_nu
	if len(nu100) != len(nuFG):
		raise CfgMismatchError("different frequency length is set in gm100 config and gmFG config")
	elif not(all(nu100 == nuFG)):
		raise CfgMismatchError("different frequency parameter is set in gm100 config and gmFG config")
	else:
		print "frequency check... OK"
		
	smooth100 = ref.bool_smooth
	smoothFG = ref.fg_smooth
	try:
		if smooth100 != smoothFG:
			raise CfgMismatchError( "warning : different smooth boolean is set in gm100 config and gmFG config")
	except CfgMismatchError as e:
		print e
	else:
		print "smooth check1... OK"

	fwhm100 = ref.fwhm
	fwhmFG = ref.fg_fwhm
	try:
		if not(all(fwhm100 == fwhmFG)):
			raise CfgMismatchError("warning : different fwhm parameter is set in gm100 config and gmFG config")
	except CfgMismatchError as e:
		print e
	else:
		print "smooth check2... OK"
	
	nside100 = ref.nside
	nsideFG = ref.fg_nside
	try:
		if nside100 != nsideFG:
			raise CfgMismatchError( "warning : different nside is set in gm100 config and gmFG config")
	except CfgMismatchError as e:
		print e
	else:
		print "nside check... OK"
	
# after running gmFG, copy foreground maps to gm100 input directory
def copy_files(new_dir, fg_name, fg_output_dir):
	new_fg_name = fg_name.replace('[nu]','*')
	if not os.path.exists(new_dir):
		os.mkdir(new_dir)
	import glob
	import shutil
	fits_files = glob.glob(new_fg_name)
	print "copy output foreground maps to gm100 input direcory..."
	for f in fits_files:
		shutil.copy(f, new_dir)
	print "copy complete!"
	
# generate foreground maps by using above functions
def generate_foreground(ref):
	check_params(ref)
	run_gmfg(ref)

	new_dir = ref.fg_input_dir
	fg_name = ref.fg_name
	fg_output_dir = ref.fg_output_dir

	copy_files(new_dir, fg_name, fg_output_dir)


if __name__ == '__main__':
	import config_params

	if len(sys.argv) == 1:
		filename = 'config.ini'
	else:
		filename = sys.argv[1]

	ref = config_params.Refs(filename)

	generate_foreground(ref)

