#!/usr/bin/env python

import numpy as np
import healpy as hp

# read Cl data and generate CMB maps
def generate_cmb(cl_s_name, cl_t_name, r, nside, seed, band):
	cl_s = np.loadtxt(cl_s_name)
	cl_t = np.loadtxt(cl_t_name)
	if len(cl_s[0]) in (4, 6): 
		cl_s = np.c_[cl_s[:,:3], np.zeros(len(cl_s))[:,np.newaxis],cl_s[:,3]]
	cls, ls = cl_s.T[1:5], cl_s.T[0]
	clt, lt = cl_t.T[1:5], cl_t.T[0]
	if len(cls[0]) > len(clt[0]):
		cl = cls[...,:len(clt[0])] + r * clt
		l = lt
	elif len(cls[0]) < len(clt[0]):
		cl = cls + r * clt[...,:len(cls[0])]
		l = ls
	else:
		cl = cls + r * clt
		l = ls
	cl = cl * 2*np.pi/(l*(l+1))
	cl = np.c_[np.zeros([cl.shape[0],2]),cl]

	np.random.seed(seed)
	cmb = np.array(hp.synfast(cl, nside = nside, new = True, pol = True, pixwin = True, verbose = False, lmax = 2*nside))
	cmb[0] = np.zeros(hp.nside2npix(nside))
	cmb_map = np.array((cmb,)*band)

	return cmb_map

# smooth CMB maps
def smooth_cmb(cmb_map, fwhm, nside):
	for i, f in enumerate(fwhm):
		cmb_map[i] = hp.smoothing(cmb_map[i], fwhm = np.radians(f/60.), pol = True, verbose = False, lmax = 2*nside, iter = 5)

# add Noise maps to CMB maps
def add_noise(cmb_map, noise, npix, seed):
	sky_map = np.zeros(cmb_map.shape)
	pix_ster = 4 * np.pi / npix 
	pix_amin = np.degrees(np.sqrt(pix_ster)) * 60.

	sigma = noise / pix_amin
	
	np.random.seed(seed)
	random_ar = np.random.randn(len(noise), 2, npix)

	random_ar *= sigma[:,np.newaxis,np.newaxis]
	sky_map[:,1,:] = cmb_map[:,1,:] + random_ar[:,0,:]
	sky_map[:,2,:] = cmb_map[:,2,:] + random_ar[:,1,:]
	return sky_map, random_ar

# add Foregroud maps to CMB maps
def add_foreground(cmb_map, nu_list, nside, fg_name):
	sky_map = np.zeros(cmb_map.shape)
	for i, nu in enumerate(nu_list):
		fg_nu_name = fg_name.replace('[nu]', str(int(nu)))
		fg = hp.read_map(fg_nu_name, nest = False, field = (1,2), verbose = False)
		fg = hp.ud_grade(fg, nside)
		sky_map[i][1] = cmb_map[i][1] + fg[0]
		sky_map[i][2] = cmb_map[i][2] + fg[1]
	return sky_map

# output maps
def output_map(map_, map_name, nu_list, arg):
	for i, nu in enumerate(nu_list):
		output_name = map_name.replace('[nu]', str(int(nu))).replace('[cnt]', str(arg))
		hp.write_map(output_name, hp.reorder(map_[i], r2n=True), nest = True, column_units = 'uK_CMB')

# output std of final maps to check
def save_std(map_, arg, name):
	map_std = np.std(map_, axis=2)
	np.save(name.replace("[cnt]",str(arg)), map_std)

# generate maps by using above functions
def gen_start(arg, ref):
	seed1 = ref.seed1 * arg
	seed2 = ref.seed2 * arg
	if ref.bool_cmb:
		print "---------------------------------"
		print "generate cmb", arg
		print "---------------------------------"
		if ref.lense:
			cl_s_name = ref.cl_lensed
		else:
			cl_s_name = ref.cl_scalar
		cl_t_name = ref.cl_tensor
		cmb =  generate_cmb(cl_s_name, cl_t_name, ref.r, ref.nside, seed1, len(ref.nu))
		if ref.output_cmb:
			print "------------------------------"
			print "###output cmb", arg
			print "------------------------------"
			output_map(cmb, ref.cmb_name, [0], arg)
		if ref.bool_smooth:
			print "---------------------------------"
			print "smoothing cmb", arg
			print "---------------------------------"
			smooth_cmb(cmb, ref.fwhm, ref.nside) 
			if ref.output_smoothed_cmb:
				print "------------------------------"
				print "###output smoothed cmb", arg
				print "------------------------------"
				output_map(cmb, ref.smoothed_cmb_name, ref.nu, arg)
	else:
		print "---------------------------------"
		print "generate zero_array", arg
		print "---------------------------------"
		cmb = np.zeros([15,3,hp.nside2npix(ref.nside)])
	
	if ref.bool_noise:
		print "---------------------------------"
		print "add noise", arg
		print "---------------------------------"
		n_cmb, noise_map = add_noise(cmb, ref.noise, hp.nside2npix(ref.nside), seed2)
		if ref.output_noise:
			print "------------------------------"
			print "###output noise", arg
			print "------------------------------"
			output_map(noise_map, ref.noise_name, ref.nu, arg)
	else:
		n_cmb = cmb

	if ref.bool_fg:
		print "---------------------------------"
		print "add foreground", arg
		print "---------------------------------"
		sky_map = add_foreground(n_cmb, ref.nu, ref.nside, ref.fg_name)
	else:
		sky_map = n_cmb

	print "---------------------------------"
	print "###output maps", arg
	print "---------------------------------"
	output_map(sky_map, ref.map_name, ref.nu, arg)

	if ref.check:
		print "---------------------------------"
		print "save cmb array data", arg
		print "---------------------------------"
		save_std(n_cmb, arg, ref.check_name)


### gen_start2 generate two maps which have
### the same CMB but different noises.
### The noises are amplified by sqrt(2) times.
### DON'T set noise_seed = 1
def gen_start2(arg, ref):
	seed1 = ref.seed1 * arg
	seed2 = ref.seed2 * arg
	if ref.bool_cmb:
		print "---------------------------------"
		print "generate cmb", arg
		print "---------------------------------"
		if ref.lense:
			cl_s_name = ref.cl_lensed
		else:
			cl_s_name = ref.cl_scalar
		cl_t_name = ref.cl_tensor
		cmb =  generate_cmb(cl_s_name, cl_t_name, ref.r, ref.nside, seed1, len(ref.nu))
		if ref.output_cmb:
			print "------------------------------"
			print "###output cmb", arg
			print "------------------------------"
			output_map(cmb, ref.cmb_name, [0], arg)
		if ref.bool_smooth:
			print "---------------------------------"
			print "smoothing cmb", arg
			print "---------------------------------"
			smooth_cmb(cmb, ref.fwhm, ref.nside) 
			if ref.output_smoothed_cmb:
				print "------------------------------"
				print "###output smoothed cmb", arg
				print "------------------------------"
				output_map(cmb, ref.smoothed_cmb_name, ref.nu, arg)
	else:
		print "---------------------------------"
		print "generate zero_array", arg
		print "---------------------------------"
		cmb = np.zeros([15,3,hp.nside2npix(ref.nside)])
	
	cmb2 = cmb.copy()
	noise = ref.noise * np.sqrt(2)
	if ref.bool_noise:
		print "---------------------------------"
		print "add noise", arg
		print "---------------------------------"
		n_cmb, noise_map = add_noise(cmb, noise, hp.nside2npix(ref.nside), seed2)
		n_cmb2, noise_map2 = add_noise(cmb2, noise, hp.nside2npix(ref.nside), seed2+1)
		if ref.output_noise:
			print "------------------------------"
			print "###output noise", arg
			print "------------------------------"
			output_map(noise_map, ref.noise_name, ref.nu, "%d-1"%arg)
			output_map(noise_map2, ref.noise_name, ref.nu, "%d-2"%arg)
		else:
			n_cmb = cmb
			n_cmb2 = cmb

	if ref.bool_fg:
		print "---------------------------------"
		print "add foreground", arg
		print "---------------------------------"
		sky_map = add_foreground(n_cmb, ref.nu, ref.nside, ref.fg_name)
		sky_map2 = add_foreground(n_cmb2, ref.nu, ref.nside, ref.fg_name)
	else:
		sky_map = n_cmb
		sky_map2 = n_cmb2

	print "---------------------------------"
	print "###output maps", arg
	print "---------------------------------"
	output_map(sky_map, ref.map_name, ref.nu, "%d-1"%arg)
	output_map(sky_map2, ref.map_name, ref.nu, "%d-2"%arg)

	if ref.check:
		print "---------------------------------"
		print "save cmb array data", arg
		print "---------------------------------"
		save_std(np.r_[n_cmb,n_cmb2], arg, ref.check_name)

if __name__=='__main__':
	import config_params

	if len(sys.argv) == 1:
		filename = 'config.ini'
	else:
		filename = sys.argv[1]

		if len(sys.argv) == 3:
			i = sys.argv[2]
		else:
			i = 1
	
	ref = config_params.Refs(filename)

	gen_start(i, ref)

