#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np
import healpy as hp
import math

import os

from ConfigParser import SafeConfigParser

h = 6.62607004 * 10**-34
k = 1.38064852 * 10**-23
hk = h/k*10**9


### part1
### calculating Intensity based on some models

# mbb
def mbb(nu,b,t):
	return nu**(b+3) / (np.exp(hk*nu/t)-1)

# pointsource power
def ps_power(freq, ref):
	planck_map_name = ref.planck_map_name
	freq_planck = ref.freq_planck
	nu = np.zeros(2)

	# find adjacent Planck frequencies (one lower [nu[0]] and higher [nu[1]])
	if freq < freq_planck[0]:
		print 'must be >', freq_planck[0], 'GHz. STOP'
		return
		
	elif freq > freq_planck[-1]:
		print 'must be <', freq_planck[-1], 'GHz. STOP'
		return

	for i in range(1,len(freq_planck)):
		if freq < freq_planck[i]:
			nu[0] = freq_planck[i-1]
			nu[1] = freq_planck[i]
			break
	# read in Planck maps
	low_name = planck_map_name.replace('[nu]', str(int(nu[0])))
	high_name = planck_map_name.replace('[nu]', str(int(nu[1])))
	map_low = np.array(hp.read_map(low_name, (0,1,2), nest = True, verbose = False))
	map_high = np.array(hp.read_map(high_name, (0,1,2), nest = True, verbose = False))
	if freq/nu[0] < nu[1]/freq: # which frequency is closer? nu[0] or nu[1]? 
		maps = map_low
		nupiv = nu[0]
	else:
		maps = map_high
		nupiv = nu[1]
	# find a power-law index when detected at both nu[0] and nu[1], and
	# compute the temperature and polarisation values at a new frequency
	# "freq" with that power-law
	ind_low  = np.nonzero(map_low[0])[0]
	ind_high = np.nonzero(map_high[0])[0]
	ind_both = np.intersect1d(ind_low, ind_high)
	beta = np.log(map_high[0,ind_both]/map_low[0,ind_both]) / np.log(nu[1]/nu[0])
	maps[:,ind_both] = maps[:,ind_both] * (freq/nupiv)**beta
	# if not detected at both nu[0] and nu[1] but only at either Planck
	# frequency, then use the average power-law index to compute the
	# temperature and polarisation values at a new frequency "freq"
	beta0 = np.average(beta)
	# detected at nu[0] but not at nu[1]
	ind_low_only = np.setdiff1d(ind_low, ind_high)
	maps[:,ind_low_only] = map_low[:,ind_low_only] * (freq/nu[0])**beta0
	# detected at nu[1] but not at nu[0]
	ind_high_only = np.setdiff1d(ind_high, ind_low)
	maps[:,ind_high_only] = map_high[:,ind_high_only] * (freq/nu[1])**beta0
	return maps

# conversion factor
def antenna2int(nu):
	nu = nu * 10**9
	return nu**2

def thermo2int(nu):
	T_cmb=2.725
	a2i = antenna2int(nu)
	nu = nu * 10**9
	x = h * nu / (k * T_cmb)
	return a2i * (x**2 * np.exp(x)) / (np.exp(x) - 1)**2

# intensity model
def Ity(nu, model, ref):
	print "   ----",nu,"----"
	# dust power-law model
	if model == 0:
		bd = ref.bd
		intensity = nu**(2+bd)
	# dust one-comp MBB model
	elif model == 1:
		bd = ref.bd
		T = ref.T
		intensity = mbb(nu, bd, T)
	# dust two-comp MBB model
	elif model == 2:
		b1 = ref.b1
		b2 = ref.b2
		T1 = ref.T1
		T2 = ref.T2
		a1 = ref.f1 * ref.qr * (1./ref.nu0)**(3+b1)
		a2 = (1-ref.f1) * (1./ref.nu0)**(3+b2)
		intensity = a1*mbb(nu,b1,T1) + a2*mbb(nu,b2,T2)
	# synch power-law model
	elif model == 10:
		bs = ref.bs
		intensity = nu**bs
	# point source power-law model
	elif model == 100:
		intensity = ps_power(nu, ref) 
	# conversion factor of antenna temperature to intnsity
	elif model == -1:
		intensity = antenna2int(nu)
	# conversion factor of thermodynamic temperature to intnsity
	elif model == -2:
		intensity = thermo2int(nu)
	return intensity

# Simpson rule
def Simpson(nu, model, bandwid, ref):
	div = ref.div
	d_nu = nu * bandwid / 2.
	hh = d_nu / div
	
	x = []
	for i in range(div * 2 + 1):
		x.append(nu - d_nu + hh * i)
	
	sum = 0.
	for cnt, nu_div in enumerate(x):
		if cnt in (0, len(x)-1):
			coe = 1
		elif (cnt % 2) == 1:
			coe = 4
		elif (cnt % 2) == 0:
			coe = 2
		sum += coe * Ity(nu_div, model, ref)
	
	s = hh / 3. * sum
	return s

# intensity band width
def Ity_Int(nu, model, bandwid, ref):
	s = Simpson(nu, model, bandwid, ref)
	return s / (2 * nu * (bandwid/2.))

# output Intensity
def Int(nu, model, band, bandwid, ref):
	if band:
		I = Ity_Int(nu, model, bandwid, ref)
	else:
		I = Ity(nu, model, ref)
	return I


### part2
### other basic functions

#### Jy/str to K_RJ or K_CMB (2k/c**2 = 1)
def g_new(nu, band, bandwid, ref):
	if ref.unit == "K_RJ":
		return 1./Int(nu, -1, band, bandwid, ref)
	elif ref.unit == "uK_RJ":
		return 10**6 * 1./Int(nu, -1, band, bandwid, ref)
	elif ref.unit == "K_CMB":
		return 1./Int(nu, -2, band, bandwid, ref)
	elif ref.unit == "uK_CMB":
		return 10**6 * 1./Int(nu, -2, band, bandwid, ref)
	

# output map
def output(Q, U, nu, fg, ref):
	nside_out = hp.get_nside(Q)
	I = np.zeros(hp.nside2npix(nside_out))
	name = ref.map_name(fg, nu, nside_out)
	
	hp.write_map(name, (I, Q, U), nest = True)
	

# generate foreground map
def foremap(dust, synch, ps, ref):
	nside = ref.nside
	nu_list = ref.nu
	fwhm_list = ref.fwhm
	nside_out = ref.nside_out
	
	print "---------------read generated maps and merge into total foreground maps--------------"
	for nu, f in zip(nu_list, fwhm_list):
		if dust:
			print "------ read dust ", nu, " -----"
			Qd, Ud = hp.read_map(ref.map_name(ref.dust_comp_name, nu, nside), field = (1, 2), nest = False)
		else:
			Qd = np.zeros(hp.nside2npix(nside))
			Ud = np.zeros(hp.nside2npix(nside))
		if synch:
			print "------ read synch ", nu, " -----"
			Qs, Us = hp.read_map(ref.map_name(ref.synchrotron_comp_name, nu, nside), field = (1, 2), nest = False)
		else:
			Qs = np.zeros(hp.nside2npix(nside))
			Us = np.zeros(hp.nside2npix(nside))
		if ps:
			print "------ read point_source ", nu, " -----"
			Qp, Up = hp.read_map(ref.map_name(ref.pointsource_comp_name, nu, nside), field = (1, 2), nest = False)
		else:
			Qp = np.zeros(hp.nside2npix(nside))
			Up = np.zeros(hp.nside2npix(nside))
			
		Qfore = Qd + Qs + Qp
		Ufore = Ud + Us + Up
		Ifore = np.zeros(hp.nside2npix(hp.get_nside(Qfore)))

		if ref.smooth:
			print "----smooth ", int(nu), "GHz map----"
			Ifore, Qfore, Ufore= hp.smoothing((Ifore, Qfore, Ufore), fwhm = np.radians(f/60.), pol = True, verbose = False)
		if ref.degrade:
			print "----degrade ", int(nu), "GHz map----"
			Ifore, Qfore, Ufore = hp.ud_grade((Ifore, Qfore, Ufore), nside_out, order_in='RING')

		print "---------output final ", int(nu), "GHz map---------"
		output(hp.reorder(Qfore, r2n = True), hp.reorder(Ufore, r2n = True), nu, ref.last_foreground_comp_name, ref)
		

### part4
### get parameters from config.ini
 
class Refs:
	def __init__(self, parser):
		main_dir = os.path.abspath(os.path.dirname(__file__))+"/../"
		input_dir = main_dir + "./input_fg/"
		templete_parser = SafeConfigParser()
		templete_parser.read(input_dir + "input_data_config_fg.ini")

		output_dir = main_dir + parser.get('output', 'output_dir')

		#### dust settings ####		
		self.dust = parser.getboolean('dust','dust')

		if self.dust:
			self.dust_ref = input_dir + templete_parser.get('dust','map_ref')
			self.dust_ref_nu = templete_parser.getfloat('dust','nu_ref')

			self.bd_name = input_dir + templete_parser.get('dust','beta')
			self.Td_name = input_dir + templete_parser.get('dust','T')
			self.T2_name = input_dir + templete_parser.get('dust','T2')
			self.T1_name = input_dir + templete_parser.get('dust','T1')

			self.dust_model = parser.getint('dust','dust_model')
			if self.dust_model > 0:
				self.dec = parser.getboolean('dust','decorr')
				if self.dec:
					dk = parser.getfloat('dust','dk')
					dec_seed = parser.getint('dust','dec_seed')
			else:
				self.dec = False

			if self.dust_model == 0:
				print "--------------read index maps--------------"
				self.bd = hp.read_map(self.bd_name, field = 0, nest = True)
			elif self.dust_model == 1:
				print "--------------read 1comp dust_parameta maps--------------"
				T_tmp = hp.read_map(self.Td_name, field = 0, nest = True)
				self.bd = hp.read_map(self.bd_name, field = 0, nest = True)
				if self.dec:
					Tq, Tu = self.addFluc(T_tmp, dk, dec_seed)
					self.T = np.vstack((Tq, Tu))
				else:
					self.T = T_tmp
			elif self.dust_model == 2:
				print "--------------read 2comp dust_parameta maps--------------"
				self.b2 = templete_parser.getfloat('dust','beta2')
				self.b1 = templete_parser.getfloat('dust','beta1')
				self.nu0 =templete_parser.getfloat('dust','nu0')
				self.f1 = templete_parser.getfloat('dust','f1')
				self.qr = templete_parser.getfloat('dust','qr')
				if self.dec:
					T2_tmp = hp.read_map(self.T2_name, field = 0, nest = True)
					T2q,T2u = self.addFluc(T2_tmp, dk, dec_seed)
					T1q = self.get_t1(T2q, self.qr, self.b1, self.b2, self.nu0)
					T1u = self.get_t1(T2u, self.qr, self.b1, self.b2, self.nu0)
					self.T2 = np.vstack((T2q, T2u))
					self.T1 = np.vstack((T1q, T1u))
				else:
					self.T2 = hp.read_map(self.T2_name, field = 0, nest = True)
					try:
						self.T1 = hp.read_map(self.T1_name, field = 0, nest = True)
					except IOError as e:
						print e
						print "generateing T1 map..."
						self.T1 = self.get_t1(self.T2, self.qr, self.b1, self.b2, self.nu0)
						print "save T1 map..."
						hp.write_map(self.T1_name, self.T1, nest=True)

			self.dust_comp_name = parser.get('dust','dust_comp_name')

		### synch settings ###
		self.synch = parser.getboolean('synch','synch')
		if self.synch:
			self.synch_ref = input_dir + templete_parser.get('synch', 'map_ref')
			self.synch_ref_nu = templete_parser.getfloat('synch', 'nu_ref')

			self.bs_name = input_dir + templete_parser.get('synch','beta')

			self.synch_model = parser.getint('synch','synch_model')
			if self.synch_model == 10:
				print "--------------read synch_parameta maps--------------"
				self.bs = hp.read_map(self.bs_name, field = 0, nest = True)

			self.synchrotron_comp_name = parser.get('synch','synchrotron_comp_name')

		### point source settings ###
		self.ps = parser.getboolean('pointsource','point_source')
		if self.ps:
			self.freq_planck = np.array([float(i) for i in templete_parser.get('pointsource','freq_planck').split()])
			self.ps_low_name = input_dir + templete_parser.get('pointsource','map_low')
			self.ps_high_name = input_dir + templete_parser.get('pointsource','map_high')

			planck_outdir = output_dir + templete_parser.get('pointsource', 'planck_output_dir')
			self.planck_map_name = planck_outdir + templete_parser.get('pointsource','planck_map_name')
			self.pol_frac = parser.getfloat('pointsource', 'pol_frac')
			self.ps_seed = parser.getint('pointsource', 'ps_seed')

			self.pointsource_comp_name = parser.get('pointsource','pointsource_comp_name')
			
		### intput parameters ###
		self.nu = np.array([float(i) for i in parser.get('input','frequency').split()])
		self.band = parser.getboolean('band','band')
		if self.band:
			self.bandwidth = np.array([float(i) for i in parser.get('band','bandwidth').split()])
			self.div = parser.getint('band', 'diviation_number')
		else:
			self.bandwidth = np.zeros(len(self.nu))
	
		self.nside = parser.getint('input','nside')
		
		### output parameters ###
		self.unit = parser.get('output','unit')
		self.name = output_dir + parser.get('output', 'name')

		self.last_foreground_comp_name = parser.get('output','last_foreground_comp_name')
		
		self.smooth = parser.getboolean('output','smooth')
		if self.smooth:
			self.fwhm = np.array([float(i) for i in parser.get('output','fwhm').split()])
			if len(self.fwhm) != len(self.nu):
				print "error : length of nu and fwhm must be the same"
				exit()
		else:
			self.fwhm = np.zeros(len(self.nu))
		self.degrade = parser.getboolean('output','degrade')
		if self.degrade:
			self.nside_out = parser.getint('output','nside_out')
		else:
			self.nside_out = self.nside

		self.smooth = parser.getboolean('output','smooth')
		self.degrade = parser.getboolean('output','degrade')
		self.fwhm = np.array([float(i) for i in parser.get('output','fwhm').split()])
		self.nside_out = parser.getint('output','nside_out')
	
	def map_name(self, comp, nu, nside):
		replaceDict = {'[comp]':comp, '[nu]':str(int(nu)), '[nside]':str(nside)}
		outname = self.name 
		for key, value in replaceDict.iteritems():
			outname = outname.replace(key, value)
		return outname

	### calculating T maps for 2comp dust model
	
	# for calculating T1
	def _zeta(self, s, n=10):
		z = np.sum(1./((np.arange(n)+1)**s))
		return z
	
	def _Z(self, alpha=None, n=10):
		z = self._zeta(4+alpha, n=n)*math.gamma(4+alpha)
		return z
	
	def get_t1(self, T2, qr, b1, b2, nu0):
		T1 = ( (1./qr) * (self._Z(b2)/self._Z(b1)) * (hk*nu0)**(b1-b2) )**(1./(4+b1)) * T2**( (4+b2)/(4+b1) )
		return T1
	
	### for decorrelation
	def addFluc(self, tmap, dk, seed):
		print " ------ add decorrelation ------ "
		npix = hp.nside2npix(hp.get_nside(tmap))
		np.random.seed(seed)
		r = np.random.randn(2,npix)
		r *= dk
		r[0][np.where(tmap<5)]=0
		r[1][np.where(tmap<5)]=0
		tq = tmap + r[0] 
		tu = tmap + r[1] 
		return tq, tu


def read_config(filename):
	main_dir = os.path.abspath(os.path.dirname(__file__))+"/../"
	parser = SafeConfigParser()
	parser.read(main_dir + filename)

	ref = Refs(parser)

	return ref


def generate(ref):
	import dust as dm
	import synch as sm
	import pointsouce as psm

	if ref.dust:
		dm.makemap(ref)
	if ref.synch:
		sm.makemap(ref)
	if ref.ps:
		psm.makemap(ref)

	foremap(ref.dust, ref.synch, ref.ps, ref)

