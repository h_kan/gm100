#!/usr/bin/env python

import numpy as np
import healpy as hp

import gen_fg

def makemap(ref):	
	nside = ref.nside
	nu = ref.nu
	nu_ref = ref.synch_ref_nu
	model = ref.synch_model
	band = ref.band
	bandwidth = ref.bandwidth
	synch_name = ref.synchrotron_comp_name
	synch_ref = ref.synch_ref

	print "--------------read synchPol maps--------------"
	Q, U = hp.read_map(synch_ref, field = (0,1), nest = True)
	Q = Q * 10**-6
	U = U * 10**-6

	Q = hp.ud_grade(Q, nside, order_in = 'NESTED')
	U = hp.ud_grade(U, nside, order_in = 'NESTED')
	print "-------- synch reference freq ---------"
	I_ref = gen_fg.Int(nu_ref, model, False, 0, ref)

	for i in range(len(nu)):
		print "-------- synch ", int(nu[i]), " GHz ---------"
		I = gen_fg.Int(nu[i], model, band, bandwidth[i], ref)
		
		g_nu = gen_fg.g_new(nu[i], band, bandwidth[i], ref)
		a = g_nu * (I/I_ref)
		a = hp.ud_grade(a, nside, order_in = 'NESTED')

		Q_nu = Q * (nu_ref * 10**9)**2 * a
		U_nu = U * (nu_ref * 10**9)**2 * a

		gen_fg.output(Q_nu, U_nu, nu[i], synch_name, ref)

