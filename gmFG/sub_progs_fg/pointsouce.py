#!/usr/bin/env python

import numpy as np
import healpy as hp
import pyfits

import gen_fg

h = 6.626 * 10**-34 
k = 1.381 * 10**-23
hk = h / k * 10**9
T_cmb = 2.725
c = 299792458

def makemap(ref):
	print"----------------------------"
	print"--------read config---------"
	print"----------------------------"
	nu = ref.nu
	planck_map_name = ref.planck_map_name

	### gen planck
	nside = ref.nside
	pol_frac = ref.pol_frac
	seed = ref.ps_seed

	print"---------------------------------------------------"
	print"--------genarate polarized Planck Source Map-------"
	print"---------------------------------------------------"
	genplanck(nside, pol_frac, seed, planck_map_name, ref)

	### gen LB
	print"--------------------------------------------"
	print"--------genarate polarized Source Map-------"
	print"--------------------------------------------"
	genLB(nu, planck_map_name, ref)


#def KRJ2mJy(nu, unit):
#	 KRJ2mJy = 2*k*nu**2 / c**2 * 10**47 # convert factor of K_RJ to mJy
#	 return KRJ2mJy/gen_fg.g(nu, unit)
def KRJ2mJy(nu):
	 KRJ2mJy = 2*k*nu**2 / c**2 * 10**47 # convert factor of K_RJ to mJy
	 return KRJ2mJy

def genplanck(nside, pol_frac, seed, planck_map_name, ref):
	#freq = [30.,44.,70.,100.,143.,217.,353.,545.]	# Planck frequencies
	freq = ref.freq_planck
	#
	# Create temperature and polarisation maps of point sources detected
	# by Planck at Nside=1024. Temperature is in units of your selected
	# at fg_config, and polarisation is created by assuming a constant
	# polarisation fraction of 0.05 and random polarisation
	# directions. The sources that exist in adjacent frequencies are
	# assigned the same polarisation directions.
	#
	npix = hp.nside2npix(nside)		# number of pixels in a map
	omega_pix = 4. * np.pi / npix	# the solid angle of a pixel
	
	# data arrays to keep track of sources appearing at different frequencies
	oldmap = np.zeros([3,npix])
	oldgamma = np.zeros(npix)
	
	for nu in freq: # loop over frequencies
		maps = np.zeros([3,npix])	# map(pixel, n) where n=0,1,2 are temperature, Stokes Q, and Stokes U, respectively
		gamma = np.zeros(npix)		# a map of polarisation directions
		print nu, ' GHz'
		if nu <= 70.:
			#d = pyfits.open('./inputmaps/COM_PCCS_%03d_R2.04.fits'%nu)[1]
			d = pyfits.open(ref.ps_low_name.replace('[nu]','%03d'%nu))[1]
		else:
			#d = pyfits.open('./inputmaps/COM_PCCS_%03d_R2.01.fits'%nu)[1]
			d = pyfits.open(ref.ps_high_name.replace('[nu]','%03d'%nu))[1]
		S = d.data['detflux']		# flux in mJy [use "detflux" in the catalogue]
		l = d.data['glon'] * np.pi/180.				# Galactic longitude in radian
		b = (d.data['glat'] + 90.) * np.pi/180.		# Galactic latitude that feeds ang2pix
		ipnest = hp.ang2pix(nside, b, l, nest = True)	# convert (l,b) to a HEALPIX pixel index in the NESTED format
		#maps[0, ipnest] = S / KRJ2mJy(nu, ref.unit) / omega_pix	# each map pixel contains units of your select
		maps[0, ipnest] = S / KRJ2mJy(nu) / omega_pix * nu**2	# each map pixel contains units of your select
		print 'number of sources=', len(ipnest)		# how many sources are there?
		noverlap = 0
		for ind in ipnest:	# compute polarisation maps from temperature
			if oldmap[0,ind] != 0:	 # same source in a lower frequency map?
				gamma[ind] = oldgamma[ind]
				noverlap = noverlap + 1
			else:						 # if not, generate pol. directions randomly
				gamma[ind] = np.pi * (np.random.random(seed) - 0.5)
			maps[1,ind] = pol_frac * maps[0, ind] * np.cos(2.*gamma[ind]) # Q
			maps[2,ind] = pol_frac * maps[0, ind] * np.sin(2.*gamma[ind]) # U
		print 'number of overlapping sources=', noverlap
		ps_nu_name = planck_map_name.replace('[nu]', str(int(nu)))
		hp.write_map(ps_nu_name, maps, nest = True)
		oldmap = maps
		oldgamma = gamma

def genLB(freq_LB, planck_map_name, ref):
	band = ref.band
	bandwid = ref.bandwidth
	pointsource_name = ref.pointsource_comp_name
	for freq, bandwid in zip(freq_LB,bandwid):	# loop over frequencies
		print "---------generating "+str(int(freq))+"GHz map--------------"
	# find adjacent Planck frequencies (one lower [nu[0]] and higher [nu[1]])
		### new
		g_nu = gen_fg.g_new(freq, band, bandwid, ref)
		maps = g_nu * gen_fg.Int(freq, 100, band, bandwid, ref)  ### pointsouce model code = 100
		gen_fg.output(maps[1], maps[2], freq, pointsource_name, ref)

