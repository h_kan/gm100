#!/usr/bin/env python

import numpy as np
import healpy as hp

import gen_fg

def makemap(ref):	
	nside = ref.nside
	model = ref.dust_model
	nu = ref.nu
	nu_ref = ref.dust_ref_nu
	band = ref.band
	bandwidth = ref.bandwidth
	dec = ref.dec
	dust_name = ref.dust_comp_name
	dust_ref = ref.dust_ref

	print "--------------read dustPol maps--------------"
	Q, U = hp.read_map(ref.dust_ref, field = (0,1), nest = True)
	Q = Q * 10**-6
	U = U * 10**-6

	Q = hp.ud_grade(Q, nside, order_in = 'NESTED')
	U = hp.ud_grade(U, nside, order_in = 'NESTED')
	print "-------- dust reference freq ---------"
	I_ref = gen_fg.Int(nu_ref, model, False, 0, ref)

	for i in range(len(nu)):
		print "-------- dust ", int(nu[i]), " GHz ---------"
		I = gen_fg.Int(nu[i], model, band, bandwidth[i], ref)

		g_nu = gen_fg.g_new(nu[i], band, bandwidth[i], ref)

		if dec:
			aq = g_nu * (I[0]/I_ref[0])
			au = g_nu * (I[1]/I_ref[1])

			aq = hp.ud_grade(aq, nside, order_in = 'NESTED')
			au = hp.ud_grade(au, nside, order_in = 'NESTED')
		else:
			a = g_nu * (I/I_ref)

			a = hp.ud_grade(a, nside, order_in = 'NESTED')
			aq = a
			au = a
	
		Q_nu = Q * (nu_ref * 10**9)**2 * aq
		U_nu = U * (nu_ref * 10**9)**2 * au
	
		gen_fg.output(Q_nu, U_nu, nu[i], dust_name, ref)
		

