#!/usr/bin/env python

import sys
from sub_progs_fg import gen_fg 


if __name__=="__main__":
	if len(sys.argv) == 1:
		filename = './config_fg.ini'
	else:
		filename = sys.argv[1]
	
	ref = gen_fg.read_config(filename)

	gen_fg.generate(ref)

	print "----------------------"
	print "output complete"
	print "----------------------"

